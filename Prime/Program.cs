﻿using System;
using System.Diagnostics;

namespace Prime
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Bitte Zahl eingeben, um alle Primzahlen bis zu dieser Zahl zu erhalten.");

            int number;
            if (int.TryParse(Console.ReadLine(), out number) == false) {
                Console.WriteLine("Not a integer :(");
                Console.ReadLine();
                return;
            }

            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();

            for (int i = 2; i <= number; i++)
            {
                if (isPrime(i))
                {
                    Console.Write(i + "\t");
                }
            }

            stopwatch.Stop();
            Console.WriteLine();
            Console.WriteLine("Time elapsed: {0:hh\\:mm\\:ss}", stopwatch.Elapsed);
            Console.ReadLine();
        }

        static bool isPrime(int number)
        {
            if (number == 1) return false;
            if (number == 2) return true;

            for (int i = 2; i <= Math.Ceiling(Math.Sqrt(number)); ++i)
            {
                if (number % i == 0) return false;
            }

            return true;
        }
    }
}